import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidationErrors } from '@angular/forms';

import { ZipValidator } from 'bsy-validation';

@Directive({
  selector: '[bgiZip]',
  providers: [
    ZipValidator,
    { provide: NG_VALIDATORS, useExisting: ZipDirective, multi: true }
  ]
})
export class ZipDirective implements Validator {

  constructor(
    private zipValidator: ZipValidator
  ) { }

  validate(control: AbstractControl): ValidationErrors | null {
    if (control.value && !this.zipValidator.validate(control.value))
      return { zip: true };
    else
      return null;
  }
}
