import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AddressLinkComponent } from './address-link/address-link.component';
import { AlertComponent } from './alert/alert.component';
import { DateInputComponent } from './date-input/date-input.component';
import { DateTimeInputComponent } from './date-time-input/date-time-input.component';
import { ErrorMessageComponent } from './error-message/error-message.component';
import { FeedbackComponent } from './invalid-feedback/feedback.component';
import { InvalidFeedbackComponent } from './invalid-feedback/invalid-feedback.component';
import { LoaderComponent } from './loader/loader.component';
import { LoadWhenDirective } from './load-when/load-when.directive';
import { PhoneInputComponent } from './phone-input/phone-input.component';
import { PhonePipe } from './phone-pipe/phone.pipe';
import { ZipDirective } from './validators/zip/zip.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    AddressLinkComponent,
    AlertComponent,
    DateInputComponent,
    DateTimeInputComponent,
    ErrorMessageComponent,
    FeedbackComponent,
    InvalidFeedbackComponent,
    LoaderComponent,
    LoadWhenDirective,
    PhoneInputComponent,
    PhonePipe,
    ZipDirective,
  ],
  exports: [
    AddressLinkComponent,
    AlertComponent,
    DateInputComponent,
    DateTimeInputComponent,
    ErrorMessageComponent,
    FeedbackComponent,
    InvalidFeedbackComponent,
    LoaderComponent,
    LoadWhenDirective,
    PhoneInputComponent,
    PhonePipe,
    ZipDirective,
  ],
  providers: [
    PhonePipe,
  ],
})
export class Bgi2UiModule { }
