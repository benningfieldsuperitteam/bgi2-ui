import {
  AfterContentInit, Component, ContentChildren, Input, OnDestroy, OnInit,
  QueryList,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

import { FeedbackComponent } from './feedback.component';

@Component({
  selector: 'bgi-invalid-feedback',
  templateUrl: './invalid-feedback.component.html',
})
export class InvalidFeedbackComponent implements AfterContentInit, OnInit, OnDestroy {
  @Input() for: FormControl;

  @ContentChildren(FeedbackComponent) feedbacks: QueryList<FeedbackComponent>;

  private statChangeSub: Subscription;

  // Subscribe to status (valid/invalid/etc.) changes on the FormControl and
  // update show or hide the feedback accordingly.
  ngOnInit(): void {
    this.statChangeSub = this.for.statusChanges
      .subscribe(() => this.updateFeedback());
  }

  // When this fires the ContentChildren (feedbacks) is set.
  ngAfterContentInit(): void {
    this.updateFeedback();
  }

  // The status changes observable never completes, so it needs to be
  // unsubscribed from manually when this component goes out of scope.
  ngOnDestroy(): void {
    if (this.statChangeSub)
      this.statChangeSub.unsubscribe();
  }

  // Show or hide the feedback based on the validity of the FormControl.
  updateFeedback(): void {
    if (this.for.invalid) {
      // FormControl has an object {[key: string]: boolean} of errors.  This
      // holds the keys (e.g. "required"/"min"/"max"/etc.).
      const errKeys = Object.keys(this.for.errors);
      let found = false;

      // Show the first feedback that has a "when" matching an error key.
      // Hide all the others.
      this.feedbacks
        .forEach(feedback => {
          const hasKey = errKeys.indexOf(feedback.when) !== -1;

          // Hide the feedback if a feedback message has been found and shown,
          // or if the key is not found (hasErrorKey could be true from the
          // last status change).
          if (found || !hasKey)
            feedback.hasErrorKey = false;
          else if (hasKey)
            feedback.hasErrorKey = found = true;
        });
    }
    else {
      // Form is valid.  Hide all feedback.
      this.feedbacks
        .forEach(feedback => feedback.hasErrorKey = false);
    }
  }
}
