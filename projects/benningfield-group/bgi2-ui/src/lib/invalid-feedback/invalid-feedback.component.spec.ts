import { Component, DebugElement } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { Bgi2UiModule } from '../bgi2-ui.module';
import { DateTimeInputComponent } from '../date-time-input/date-time-input.component';
import { InvalidFeedbackComponent } from './invalid-feedback.component';

describe('InvalidFeedbackComponent()', () => {
  // Form component for testing feedback on a model.
  @Component({
    selector: 'bgi-test-feedback-form',
    template: `
      <form name="frmAppt" #frm="ngForm" novalidate>
        <bgi-date-time-input name="dtInput" #dtInput="ngModel"
          [(ngModel)]="datetime" timezone="America/Adak" required>
        </bgi-date-time-input>

        <bgi-invalid-feedback [for]="dtInput">
          <bgi-feedback when="datetime">Invalid date-time.</bgi-feedback>
          <bgi-feedback when="required">Required.</bgi-feedback>
        </bgi-invalid-feedback>
      </form>
    `,
  })
  class TestFeedbackForm {}

  let testFormFixture: ComponentFixture<TestFeedbackForm>;
  let invalidFeedback: InvalidFeedbackComponent;
  let dtInput: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        FormsModule,
        Bgi2UiModule,
      ],
      declarations: [
        TestFeedbackForm,
      ],
    }).compileComponents();

    testFormFixture = TestBed.createComponent(TestFeedbackForm);
    testFormFixture.autoDetectChanges(true);

    invalidFeedback = testFormFixture.debugElement
      .query(By.directive(InvalidFeedbackComponent))
      .componentInstance;

    dtInput = testFormFixture.debugElement
      .query(By.directive(DateTimeInputComponent));
  });

  describe('.constructor()', () => {
    it('initializes.', () => {
      expect(invalidFeedback).toBeDefined();
    });
  });

  describe('.updateFeedback()', () => {
    it('initially shows the required error when the input is empty.', () => {
      // datetime then required.
      expect(invalidFeedback.feedbacks.first.hasErrorKey).toBe(false);
      expect(invalidFeedback.feedbacks.last.hasErrorKey).toBe(true);
    });

    it('shows the first matching error and hides the rest.', () => {
      // Month is valid but everything else is missing.  Errors will be required and datetime.
      const monthEle: HTMLInputElement = dtInput.query(By.css('input')).nativeElement;

      monthEle.value = '1';
      monthEle.dispatchEvent(new Event('input'));

      expect(invalidFeedback.feedbacks.first.hasErrorKey).toBe(true);
      expect(invalidFeedback.feedbacks.last.hasErrorKey).toBe(false);
    });

    it('hides the errors when the form control is valid.', () => {
      const inputs: HTMLInputElement[] = dtInput.queryAll(By.css('input'))
        .map(debugEle => debugEle.nativeElement);

      inputs[0].value = '1';
      inputs[1].value = '1';
      inputs[2].value = '2020';
      inputs[3].value = '0';
      inputs[4].value = '0';

      inputs.forEach(input => input.dispatchEvent(new Event('input')));

      expect(invalidFeedback.feedbacks.first.hasErrorKey).toBe(false);
      expect(invalidFeedback.feedbacks.last.hasErrorKey).toBe(false);
    });
  });
});
