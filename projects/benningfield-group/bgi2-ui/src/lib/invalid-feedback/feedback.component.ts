import { Component, Input } from '@angular/core';

@Component({
  selector: 'bgi-feedback',
  templateUrl: './feedback.component.html',
})
export class FeedbackComponent {
  @Input() when: string;

  hasErrorKey: boolean = false;
}
