import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bgi-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})

export class AlertComponent {
  @Input() show: boolean;
  @Input() title = 'Alert';
  @Output() klose: EventEmitter<void> = new EventEmitter();

  constructor() {
  }

  onClose(): void {
    this.show = false;
    this.klose.emit();
  }
}

