import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import {
  ControlValueAccessor,
  Validator,
  ValidationErrors,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
} from '@angular/forms';

import { Moment } from 'moment-timezone';
import * as momentNS from 'moment-timezone';

const moment = momentNS;

@Component({
  selector: 'bgi-date-input',
  templateUrl: './date-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => DateInputComponent)
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: forwardRef(() => DateInputComponent)
    }
  ]
})
export class DateInputComponent implements ControlValueAccessor, Validator {
  @Input() timezone: string;

  private date: Date;
  private touched: Set<string> = new Set();

  day: string;
  month: string;
  year: string;

  disabled = false;
  onChange: (_: any) => void;
  onTouch: () => void;

  constructor() {}

  writeValue(date: Date): void {
    if (!this.timezone)
      throw new Error('Timezone is a required Input in the DateInputComponent.');

    this.date = date;

    if (this.date) {
      const momentDate: Moment = moment(date).tz(this.timezone);

      this.month = (momentDate.month() + 1).toString().padStart(2, '0');
      this.day   = momentDate.date().toString().padStart(2, '0');
      this.year  = momentDate.year().toString();
    }
    else {
      this.month = this.day = this.year = null;
    }

    if (this.onChange)
      this.onChange(this.date);
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  private parseDate(): Date {
    const momentDate: Moment = moment.tz(`${this.year}-${this.month}-${this.day}`, 'YYYY-MM-DD', this.timezone);

    return new Date(
      momentDate.year(),
      momentDate.month(),
      momentDate.date());
  }

  validate(): ValidationErrors {
    const valErrors = {};

    // If at least one of the fields are missing.
    if ((!this.month || !this.day || !this.year)) {
      // If all of the fields are missing, return null.
      if (!this.month && !this.day && !this.year) {
        return null;
      }
      // Else, set the missing fields in the valErrors object.
      else {
        valErrors['date'] = true;

        if (!this.month) valErrors['month'] = true;
        if (!this.day) valErrors['day'] = true;
        if (!this.year) valErrors['year'] = true;
      }
    }
    else {
      const monthInt = +this.month;
      const dayInt = +this.day;
      const yearInt = +this.year;

      if (isNaN(monthInt) || monthInt < 1 || monthInt > 12)
        valErrors['month'] = true;

      if (isNaN(dayInt) || dayInt < 1 || dayInt > 31)
        valErrors['day'] = true;

      if (isNaN(yearInt) || yearInt < 1 || yearInt > 9999)
        valErrors['year'] = true;

      // Case for entering things like 2/31/2018, which is invalid
      // and transformed to March 3, 2018.
      const parsedDate = this.parseDate();
      if (isNaN(parsedDate.valueOf()) ||
        valErrors['month'] ||
        valErrors['day'] ||
        valErrors['year'])

        valErrors['date'] = true;
    }

    return Object.keys(valErrors).length === 0 ? null : valErrors;
  }

  createDate() {
    if (!this.month && !this.day && !this.year)
      this.date = null;
    else if (!this.month || !this.day || !this.year)
      this.date = undefined;
    else if (this.validate() !== null)
      this.date = undefined;
    else
      this.date = this.parseDate();

    this.onChange(this.date);
  }

  onBlur(e: Event): void {
    const targetName: string = (e.target as HTMLInputElement).name;

    if (!this.touched.has(targetName)) {
      this.touched.add(targetName);
    }

    // All three fields--month, day, and year--touched, or there is a date
    // and one fields was touched.
    if (this.onTouch) {
      if (this.touched.size === 3 || this.month && this.day && this.year)
        this.onTouch();
    }
  }
}
