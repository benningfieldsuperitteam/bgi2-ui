import { TestBed } from '@angular/core/testing';

import { DateInputComponent } from './date-input.component';

describe('DateInputComponent', () => {
  let component: DateInputComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DateInputComponent]
    });

    component = TestBed.inject(DateInputComponent);

    component.timezone = 'America/New_York';
  });

  describe('.constructor()', () => {
    it('should create.', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('.writeValue()', () => {
    it('stores the month, day, and year if a Date instance is supplied.', () => {
      const date = new Date('2018-09-01T00:00:00.000Z');

      component.writeValue(date);

      expect(component.month).toBe('08');
      expect(component.day).toBe('31');
      expect(component.year).toBe('2018');
    });

    it('correctly sets the month, day, year for various timezones.', () => {
      component.timezone = 'Pacific/Honolulu';

      const date = new Date('2018-09-01T07:00:00.000Z');

      component.writeValue(date);

      expect(component.month).toBe('08');
      expect(component.day).toBe('31');
      expect(component.year).toBe('2018');


      component.timezone = 'America/Los_Angeles';

      component.writeValue(date);

      expect(component.month).toBe('09');
      expect(component.day).toBe('01');
      expect(component.year).toBe('2018');

      component.timezone = 'UTC';

      component.writeValue(date);

      expect(component.month).toBe('09');
      expect(component.day).toBe('01');
      expect(component.year).toBe('2018');
    });

    it('sets the month, day, and year to null if no Date is supplied.', () => {
      const date = new Date(2018, 8, 1);

      component.writeValue(date);
      component.writeValue(null);

      expect(component.month).toBe(null);
      expect(component.day).toBe(null);
      expect(component.year).toBe(null);
    });

    it('calls onChange after writing the date.', () => {
      let locDate: Date;

      component.registerOnChange((d: Date) => locDate = d);

      const date = new Date(2018, 8, 1);
      component.writeValue(date);

      expect(locDate).toBe(date);
    });
  });

  describe('.validate()', () => {
    it('returns null if there is no month, day, and year.', () => {
      expect(component.validate()).toBeNull();
    });

    it('fails validation if only one or two fields are filled in.', () => {
      expect(component.validate()).toBeNull();

      component.month = '01';
      expect(component.validate().month).toBeUndefined();
      expect(component.validate().day).toBe(true);
      expect(component.validate().year).toBe(true);
      component.month = undefined;

      component.day = '20';
      expect(component.validate().month).toBe(true);
      expect(component.validate().day).toBeUndefined();
      expect(component.validate().year).toBe(true);
      component.day = undefined;

      component.year = '2020';
      expect(component.validate().month).toBe(true);
      expect(component.validate().day).toBe(true);
      expect(component.validate().year).toBeUndefined();
      component.year = undefined;

      component.month = '01';
      component.day = '20';
      expect(component.validate().month).toBeUndefined();
      expect(component.validate().day).toBeUndefined();
      expect(component.validate().year).toBe(true);
      component.month = undefined;
      component.day = undefined;

      component.month = '01';
      component.year = '2020';
      expect(component.validate().month).toBeUndefined();
      expect(component.validate().day).toBe(true);
      expect(component.validate().year).toBeUndefined();
      component.month = undefined;
      component.year = undefined;

      component.day = '20';
      component.year = '2020';
      expect(component.validate().month).toBe(true);
      expect(component.validate().day).toBeUndefined();
      expect(component.validate().year).toBeUndefined();
      component.day = undefined;
      component.year = undefined;
    });

    it('returns null if the month, day, and year form a valid date.', () => {
      component.month = '01';
      component.day   = '02';
      component.year  = '2018';

      expect(component.validate()).toBeNull();
    });

    it('fails validation if the month is less than 1.', () => {
      component.month = '-1';
      component.day   = '02';
      component.year  = '2018';

      expect(component.validate().month).toBe(true);
      expect(component.validate().date).toBe(true);
    });

    it('fails validation if the month is greater than 12.', () => {
      component.month = '13';
      component.day   = '02';
      component.year  = '2018';

      expect(component.validate().month).toBe(true);
      expect(component.validate().date).toBe(true);
    });

    it('fails validation if the day is less than 1.', () => {
      component.month = '01';
      component.day   = '00';
      component.year  = '2018';

      expect(component.validate().day).toBe(true);
      expect(component.validate().date).toBe(true);
    });

    it('fails validation if the day is greater than 31.', () => {
      component.month = '01';
      component.day   = '32';
      component.year  = '2018';

      expect(component.validate().day).toBe(true);
      expect(component.validate().date).toBe(true);
    });

    it('fails validation if the year is less than 1.', () => {
      component.month = '01';
      component.day   = '02';
      component.year  = '0';

      expect(component.validate().year).toBe(true);
      expect(component.validate().date).toBe(true);
    });

    it('fails if the date is invalid.', () => {
      // Feb 30, 2018 is invalid.
      component.month = '02';
      component.day   = '30';
      component.year  = '2018';

      expect(component.validate().date).toBe(true);
    });
  });
});
