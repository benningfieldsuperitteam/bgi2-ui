import { Component, forwardRef, Output, EventEmitter, Input } from '@angular/core';
import {
  ControlValueAccessor,
  Validator,
  ValidationErrors,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
} from '@angular/forms';

import { Moment } from 'moment-timezone';
import * as momentNS from 'moment-timezone';

const moment = momentNS;

import { DateTimeInputValidationErrors } from './date-time-input-validation-errors';

@Component({
  selector: 'bgi-date-time-input',
  templateUrl: './date-time-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => DateTimeInputComponent)
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: forwardRef(() => DateTimeInputComponent)
    }
  ]
})
export class DateTimeInputComponent implements ControlValueAccessor, Validator {
  @Input() timezone: string;

  private datetime: Date;
  private touched: Set<string> = new Set();
  hour: string;
  minute: string;
  meridiem: string;

  disabled = false;
  onChange: (_: any) => void;
  onTouch: () => void;

  day: string;
  month: string;
  year: string;

  valErrors: DateTimeInputValidationErrors;

  constructor() { }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  writeValue(datetime: Date): void {
    if (!this.timezone)
      throw new Error('Timezone is a required Input in the DateInputComponent.');

    this.datetime = datetime;

    if (this.datetime) {
      const momentDate: Moment = moment(datetime).tz(this.timezone);

      this.month = (momentDate.month() + 1).toString().padStart(2, '0');
      this.day = momentDate.date().toString().padStart(2, '0');
      this.year = momentDate.year().toString();
      this.hour = momentDate.format('hh').padStart(2, '0');
      this.minute = momentDate.minute().toString().padStart(2, '0');
      this.meridiem = momentDate.format('A');
    }
    else {
      this.month = this.day = this.year = this.hour = this.minute = null;
      this.meridiem = 'AM';
    }

    if (this.onChange)
      this.onChange(this.datetime);
  }

  // Validate the input fields.
  validate(): ValidationErrors {
    this.valErrors = new DateTimeInputValidationErrors();

    if ((!this.month || !this.day || !this.year || !this.hour || !this.minute)) {
      if (!this.month && !this.day && !this.year && !this.hour && !this.minute)
        return null;
      else {
        this.valErrors.datetime = true;

        if (!this.month) this.valErrors.month = true;
        if (!this.day) this.valErrors.day = true;
        if (!this.year) this.valErrors.year = true;
        if (!this.hour) this.valErrors.hour = true;
        if (!this.minute) this.valErrors.minute = true;
      }
    }
    else {
      const monthInt = +this.month;
      const dayInt = +this.day;
      const yearInt = +this.year;
      const hourInt = +this.hour;
      const minuteInt = +this.minute;

      if (isNaN(monthInt) || monthInt < 1 || monthInt > 12)
        this.valErrors.month = true;

      if (isNaN(dayInt) || dayInt < 1 || dayInt > 31)
        this.valErrors.day = true;

      if (isNaN(yearInt) || yearInt < 1 || yearInt > 9999)
        this.valErrors.year = true;

      if (isNaN(hourInt) || hourInt < 0 || hourInt > 12)
        this.valErrors.hour = true;

      if (isNaN(minuteInt) || minuteInt < 0 || minuteInt > 59)
        this.valErrors.minute = true;

      const parsedDate = this.parseDatetime();

      if (!parsedDate || this.valErrors.month || this.valErrors.day ||
        this.valErrors.year || this.valErrors.hour || this.valErrors.minute)
        this.valErrors.datetime = true;
    }

    return Object.keys(this.valErrors).length === 0 ? null : this.valErrors;
  }

  // Turn input fields into a Date object.
  parseDatetime(): Date {
    const datetimeStr = `${this.year}-${this.month}-${this.day} ${this.hour}:${this.minute} ${this.meridiem}`;
    const momentDate: Moment = moment.tz(datetimeStr, 'YYYY-MM-DD hh:mm A', this.timezone);

    return momentDate.isValid() ? momentDate.toDate() : null;
  }

  // Sets the datetime property.
  createDatetime() {
    if (!this.month && !this.day && !this.year && !this.hour && !this.minute)
      this.datetime = null;
    else if (!this.month || !this.day || !this.year || !this.hour || !this.minute)
      this.datetime = undefined;
    else if (this.validate() !== null)
      this.datetime = undefined;
    else
      this.datetime = this.parseDatetime();

    this.onChange(this.datetime);
  }

  onBlur(e: Event): void {
    const targetName: string = (e.target as HTMLInputElement).name;

    if (!this.touched.has(targetName)) {
      this.touched.add(targetName);
    }

    // All six fields--month, day, year, hour, minute --touched, or
    // there is a date and one field was touched.
    if (this.onTouch) {
      if (this.touched.size === 6 || this.month && this.day && this.year && this.hour && this.minute)
        this.onTouch();
    }
  }
}
