import { TestBed } from '@angular/core/testing';

import { DateTimeInputComponent } from './date-time-input.component';

import * as moment from 'moment-timezone';

describe('DateTimeInputComponent', () => {
  let component: DateTimeInputComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DateTimeInputComponent]
    });

    component = TestBed.inject(DateTimeInputComponent);

    component.timezone = 'America/New_York';
  });

  describe('.constructor()', () => {
    it('should create.', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('.writeValue()', () => {
    it('stores the month, day, year, hour, minute, meridiem if a Date instance is supplied.', () => {
      const date = new Date('2020-02-03T00:00:00.000Z');

      component.writeValue(date);

      expect(component.month).toBe('02');
      expect(component.day).toBe('02');
      expect(component.year).toBe('2020');
      expect(component.hour).toBe('07');
      expect(component.minute).toBe('00');
      expect(component.meridiem).toBe('PM');
    });

    it('correctly sets the month, day, year for various timezones.', () => {
      component.timezone = 'Pacific/Honolulu';

      const date = new Date('2020-02-03T00:00:00.000Z');

      component.writeValue(date);

      expect(component.month).toBe('02');
      expect(component.day).toBe('02');
      expect(component.year).toBe('2020');
      expect(component.hour).toBe('02');
      expect(component.minute).toBe('00');
      expect(component.meridiem).toBe('PM');


      component.timezone = 'America/Los_Angeles';

      component.writeValue(date);

      expect(component.month).toBe('02');
      expect(component.day).toBe('02');
      expect(component.year).toBe('2020');
      expect(component.hour).toBe('04');
      expect(component.minute).toBe('00');
      expect(component.meridiem).toBe('PM');

      component.timezone = 'UTC';

      component.writeValue(date);

      expect(component.month).toBe('02');
      expect(component.day).toBe('03');
      expect(component.year).toBe('2020');
      expect(component.hour).toBe('12');
      expect(component.minute).toBe('00');
      expect(component.meridiem).toBe('AM');
    });

    it('sets the month, day, year, hour, minute, and meridiem to null if no Date is supplied.', () => {
      const date = new Date('2018-09-01T00:00:00.000Z');

      component.writeValue(date);
      component.writeValue(null);

      expect(component.month).toBe(null);
      expect(component.day).toBe(null);
      expect(component.year).toBe(null);
    });

    it('calls onChange after writing the date.', () => {
      let locDate: Date;

      component.registerOnChange((d: Date) => locDate = d);

      const date = new Date(2018, 8, 1);
      component.writeValue(date);

      expect(locDate).toBe(date);
    });
  });

  describe('.parseDatetime()', () => {
    it('returns null if the datetime is invalid.', () => {
      component.month = '02';
      component.day = '31';
      component.year = '2020';
      component.hour = '11';
      component.minute = '39';
      component.meridiem = 'AM';

      expect(component.parseDatetime()).toBeNull();
      component.day = '03';

      component.minute = '61';
      expect(component.parseDatetime()).toBeNull();
      component.minute = '39';
    });

    it('returns a datetime if the datetime is valid.', () => {
      component.month = '02';
      component.day = '03';
      component.year = '2020';
      component.hour = '11';
      component.minute = '39';
      component.meridiem = 'AM';

      const momentDate = moment(component.parseDatetime()).tz(component.timezone);
      expect(momentDate.toISOString()).toBe('2020-02-03T16:39:00.000Z');
    });
  });

  describe('.validate()', () => {
    it('returns null if there is no month, day, year, hour, minute, and meridiem.', () => {
      expect(component.validate()).toBeNull();
    });

    it('fails validation if not all fields are filled in.', () => {
      expect(component.validate()).toBeNull();

      component.month = '01';
      let validation = component.validate();
      expect(validation.month).toBeUndefined();
      expect(validation.day).toBe(true);
      expect(validation.year).toBe(true);
      expect(validation.hour).toBe(true);
      expect(validation.minute).toBe(true);
      component.month = undefined;

      component.day = '20';
      validation = component.validate();
      expect(validation.month).toBe(true);
      expect(validation.day).toBeUndefined();
      expect(validation.year).toBe(true);
      expect(validation.hour).toBe(true);
      expect(validation.minute).toBe(true);
      component.day = undefined;

      component.year = '2020';
      validation = component.validate();
      expect(validation.month).toBe(true);
      expect(validation.day).toBe(true);
      expect(validation.year).toBeUndefined();
      expect(validation.hour).toBe(true);
      expect(validation.minute).toBe(true);
      component.year = undefined;

      component.month = '01';
      component.day = '20';
      validation = component.validate();
      expect(validation.month).toBeUndefined();
      expect(validation.day).toBeUndefined();
      expect(validation.year).toBe(true);
      expect(validation.hour).toBe(true);
      expect(validation.minute).toBe(true);
      component.month = undefined;
      component.day = undefined;

      component.month = '01';
      component.year = '2020';
      validation = component.validate();
      expect(validation.month).toBeUndefined();
      expect(validation.day).toBe(true);
      expect(validation.year).toBeUndefined();
      expect(validation.hour).toBe(true);
      expect(validation.minute).toBe(true);
      component.month = undefined;
      component.year = undefined;

      component.day = '20';
      component.year = '2020';
      validation = component.validate();
      expect(validation.month).toBe(true);
      expect(validation.day).toBeUndefined();
      expect(validation.year).toBeUndefined();
      expect(validation.hour).toBe(true);
      expect(validation.minute).toBe(true);
      component.day = undefined;
      component.year = undefined;

      component.day = '20';
      component.year = '2020';
      validation = component.validate();
      expect(validation.month).toBe(true);
      expect(validation.day).toBeUndefined();
      expect(validation.year).toBeUndefined();
      expect(validation.hour).toBe(true);
      expect(validation.minute).toBe(true);
      component.day = undefined;
      component.year = undefined;

      component.hour = '01';
      validation = component.validate();
      expect(validation.month).toBe(true);
      expect(validation.day).toBe(true);
      expect(validation.year).toBe(true);
      expect(validation.hour).toBeUndefined();
      expect(validation.minute).toBe(true);
      component.hour = undefined;

      component.hour = '01';
      component.minute = '55';
      validation = component.validate();
      expect(validation.month).toBe(true);
      expect(validation.day).toBe(true);
      expect(validation.year).toBe(true);
      expect(validation.hour).toBeUndefined();
      expect(validation.minute).toBeUndefined();
      component.hour = undefined;
      component.minute = undefined;

      component.hour = '01';
      component.meridiem = 'AM';
      validation = component.validate();
      expect(validation.month).toBe(true);
      expect(validation.day).toBe(true);
      expect(validation.year).toBe(true);
      expect(validation.hour).toBeUndefined();
      expect(validation.minute).toBe(true);
      component.hour = undefined;
      component.meridiem = undefined;

      component.minute = '30';
      component.meridiem = 'PM';
      validation = component.validate();
      expect(validation.month).toBe(true);
      expect(validation.day).toBe(true);
      expect(validation.year).toBe(true);
      expect(validation.hour).toBe(true);
      expect(validation.minute).toBeUndefined();
      component.minute = undefined;
      component.meridiem = undefined;
    });

    it('returns null if the month, day, year, hour, minute, and meridiem form a valid datetime.', () => {
      component.month = '02';
      component.day = '03';
      component.year = '2020';
      component.hour = '03';
      component.minute = '39';
      component.meridiem = 'PM';
      expect(component.validate()).toBeNull();
    });

    it('fails validation if the month is less than 1.', () => {
      component.month = '00';
      component.day = '03';
      component.year = '2020';
      component.hour = '03';
      component.minute = '39';
      component.meridiem = 'PM';
      expect(component.validate().month).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the month is greater than 12.', () => {
      component.month = '13';
      component.day = '03';
      component.year = '2020';
      component.hour = '03';
      component.minute = '39';
      component.meridiem = 'PM';

      expect(component.validate().month).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the day is less than 1.', () => {
      component.month = '02';
      component.day = '00';
      component.year = '2020';
      component.hour = '03';
      component.minute = '39';
      component.meridiem = 'PM';

      expect(component.validate().day).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the day is greater than 31.', () => {
      component.month = '02';
      component.day = '32';
      component.year = '2020';
      component.hour = '03';
      component.minute = '39';
      component.meridiem = 'PM';

      expect(component.validate().day).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the year is less than 1.', () => {
      component.month = '02';
      component.day = '03';
      component.year = '0';
      component.hour = '03';
      component.minute = '39';
      component.meridiem = 'PM';


      expect(component.validate().year).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the year is less than 1.', () => {
      component.month = '02';
      component.day = '03';
      component.year = '0';
      component.hour = '03';
      component.minute = '39';
      component.meridiem = 'PM';


      expect(component.validate().year).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the hour is less than 0.', () => {
      component.month = '02';
      component.day = '03';
      component.year = '2020';
      component.hour = '00';
      component.minute = '39';
      component.meridiem = 'PM';

      expect(component.validate()).toBe(null);

      component.hour = '-01';
      expect(component.validate().hour).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the hour is greater than 12', () => {
      component.month = '02';
      component.day = '03';
      component.year = '2020';
      component.hour = '13';
      component.minute = '39';
      component.meridiem = 'PM';

      expect(component.validate().hour).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the minute is less than 0.', () => {
      component.month = '02';
      component.day = '03';
      component.year = '2020';
      component.hour = '00';
      component.minute = '00';
      component.meridiem = 'PM';

      expect(component.validate()).toBe(null);

      component.minute = '-01';
      expect(component.validate().minute).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails validation if the minute is greater than 59', () => {
      component.month = '02';
      component.day = '03';
      component.year = '2020';
      component.hour = '03';
      component.minute = '60';
      component.meridiem = 'PM';

      expect(component.validate().minute).toBe(true);
      expect(component.validate().datetime).toBe(true);
    });

    it('fails if the date is invalid.', () => {
      component.month = '02';
      component.day = '30';
      component.year = '2020';
      component.hour = '03';
      component.minute = '39';
      component.meridiem = 'PM';

      expect(component.validate().datetime).toBe(true);
    });
  });
});

