export class DateTimeInputValidationErrors {
  year: boolean;
  month: boolean;
  day: boolean;
  hour: boolean;
  minute: boolean;
  meridiem: boolean;
  datetime: boolean;
}
