import { Component, Input, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'bgi-error-message',
  templateUrl: './error-message.component.html'
})
export class ErrorMessageComponent implements OnChanges {
  @Input() when: Observable<any>;
  messages: string[] = null;

  ngOnChanges(): void {
    if (this.when && !(this.when instanceof Observable))
      throw new Error('bgi-error-message used without an observable.');

    this.messages = null;

    if (this.when) {
      this.when
        .subscribe({
          error: (err: any) => this.messages = this.normalizeError(err)
        });
    }
  }

  normalizeError(err: any): string[] {
    if (typeof err === 'string')
      return [err];

    if (err !== null && typeof err === 'object') {
      // HttpErrorResponse or something where the error is nested.
      if (err.error)
        return this.normalizeError(err.error);

      // An error with nested errors, e.g. a ValidationErrorList.
      if (err.errors && Array.isArray(err.errors) && err.errors.length > 0) {
        return err.errors
          .reduce(
            (messages, e) => messages.concat(this.normalizeError(e)),
            []
          );
      }

      // DetaildError from bsy-error.
      if (err.detail)
        return [err.detail];

      // Standard Error instance.
      if (err.message)
        return [err.message];
    }

    // Can't normalize.
    return ['An unknown error occurred.'];
  }
}
