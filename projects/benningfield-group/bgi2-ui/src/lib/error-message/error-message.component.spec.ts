import { TestBed } from '@angular/core/testing';

import { of, throwError } from 'rxjs';

import { ErrorMessageComponent } from './error-message.component';

describe('ErrorMessageComponent()', () => {
  let component: ErrorMessageComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ErrorMessageComponent,
      ],
    });

    component = TestBed.inject(ErrorMessageComponent);
  });

  describe('.normalizeError()', () => {
    it('returns the string.', () => {
      expect(component.normalizeError('fail')).toEqual(['fail']);
    });

    it('normalizes the nested error.', () => {
      const err = {error: {message: 'fail'}};

      expect(component.normalizeError(err)).toEqual(['fail']);
    });

    it('normalizes nested errors.', () => {
      const err = {
        errors: [
          {message: 'fail1'},
          {detail: 'fail2'},
          new Error('fail3'),
        ]
      };

      expect(component.normalizeError(err)).toEqual(['fail1', 'fail2', 'fail3']);
    });

    it('returns the detail.', () => {
      const err = {detail: 'fail'};

      expect(component.normalizeError(err)).toEqual(['fail']);
    });

    it('returns the message.', () => {
      const err = {message: 'fail'};

      expect(component.normalizeError(err)).toEqual(['fail']);
    });
  });

  describe('.ngOnChanges()', () => {
    it('subscribes to the observable and normalizes any errors pushed through it.', () => {
      component.when = throwError(new Error('fail'));
      component.ngOnChanges();
      expect(component.messages).toEqual(['fail']);

      component.when = throwError({detail: 'fail'});
      component.ngOnChanges();
      expect(component.messages).toEqual(['fail']);

      component.when = throwError({
        error: {
          errors: [
            {detail: 'fail1'},
            {message: 'fail2'},
            new Error('fail3'),
          ]
        }
      });

      component.ngOnChanges();
      expect(component.messages).toEqual(['fail1', 'fail2', 'fail3']);
    });
  });
});
