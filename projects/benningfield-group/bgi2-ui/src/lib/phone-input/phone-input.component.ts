import { Component, forwardRef, Input, } from '@angular/core';
import {
  ControlValueAccessor, Validator, ValidationErrors, NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
} from '@angular/forms';

import { PhoneValidator } from 'bsy-validation';

import { PhonePipe } from '../phone-pipe/phone.pipe';

@Component({
  selector: 'bgi-phone-input',
  templateUrl: './phone-input.component.html',
  providers: [
    PhoneValidator,
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => PhoneInputComponent)
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: forwardRef(() => PhoneInputComponent)
    },
  ]
})
export class PhoneInputComponent implements ControlValueAccessor, Validator {
  @Input() name: string;

  constructor(
    private phonePipe: PhonePipe,
    private phoneValidator: PhoneValidator) {
  }

  private onChange: (_: any) => void;
  private onTouch: () => void;

  disabled: boolean = false;
  phone: string;

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouch = fn;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  // This fires if the parent component passes in a phone number via ngModel
  // and transforms (formats) the phone number using the PhonePipe.
  writeValue(phone: string): void {
    if (phone)
      this.phone = this.phonePipe.transform(phone);
    else
      this.phone = phone;
  }

  // Validate the phone number as a US phone.
  validate(): ValidationErrors {
    if (this.phone === '' || this.phoneValidator.validate(this.phone))
      return null;

    return {phone: true};
  }

  // Register the component as touch when the focus leaves the phone input field.
  doTouch(e: Event): void {
    if (this.onTouch)
      this.onTouch();
  }

  // Propagate phone up to ngModel.  The model is undefined if it's invalid.
  // This is called on "input" (wired up in the template).
  doChange(): void {
    if (!this.onChange)
      return;

    if (!this.phone)
      this.onChange(null);
    else if (this.validate() === null)
      this.onChange(this.phone);
    else
      this.onChange(undefined);
  }
}
