import { TestBed } from '@angular/core/testing';

import { PhoneValidator } from 'bsy-validation';

import { Bgi2UiModule } from '../bgi2-ui.module';

import { PhoneInputComponent } from './phone-input.component';

describe('PhoneInputComponent()', () => {
  let component: PhoneInputComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [Bgi2UiModule],
      providers: [
        PhoneInputComponent,
        PhoneValidator,
      ],
    });

    component = TestBed.inject(PhoneInputComponent);
  });

  describe('.writeValue()', () => {
    it('formats the phone number.', () => {
      component.writeValue('98765432122');
      expect(component.phone).toBe('(987) 654-3212 ext. 2');
    });

    it('keeps phone as is if null or undefined is passed in.', () => {
      component.writeValue(null);
      expect(component.phone).toBe(null);

      component.writeValue(undefined);
      expect(component.phone).toBe(undefined);
    });
  });

  describe('.validate()', () => {
    it('validates the phone number.', () => {
      component.phone = '9162241111';
      expect(component.validate()).toBe(null);
      component.phone = '(916)2241111';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-2241111';
      expect(component.validate()).toBe(null);
      component.phone = '(916).2241111';
      expect(component.validate()).toBe(null);
      component.phone = '(916) 2241111';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224-1111';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224.1111';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224 1111';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224 1111 12';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224 1111 e12';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224 1111 ex12';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224 1111 ext12';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224 1111 ext.12';
      expect(component.validate()).toBe(null);
      component.phone = '(916)-224 1111 ext. 12';
      expect(component.validate()).toBe(null);

      component.phone = '';
      expect(component.validate()).toEqual(null);
      component.phone = undefined;
      expect(component.validate()).toEqual(null);
      component.phone = null;
      expect(component.validate()).toEqual(null);

      component.phone = '1 234 456 9900 ext. 21';
      expect(component.validate()).toEqual({phone: true});
      component.phone = '1 916-224-1111';
      expect(component.validate()).toEqual({phone: true});

      component.phone = 'test';
      expect(component.validate()).toEqual({phone: true});
      component.phone = ' 9162241111';
      expect(component.validate()).toEqual({phone: true});
      component.phone = '916  2241111';
      expect(component.validate()).toEqual({phone: true});
      component.phone = '916224  1111';
      expect(component.validate()).toEqual({phone: true});
      component.phone = '911';
      expect(component.validate()).toEqual({phone: true});
    });
  });

  describe('.doChange()', () => {
    let changeSpy: jasmine.Spy;

    beforeEach(() => {
      changeSpy = jasmine.createSpy('onChange');

      component.registerOnChange(changeSpy);
    });

    it('propagates null if phone is falsey.', () => {
      component.phone = '';
      component.doChange();

      expect(changeSpy).toHaveBeenCalledWith(null);
    });

    it('propagates undefined if the phone number is invalid.', () => {
      component.phone = '911';
      component.doChange();

      expect(changeSpy).toHaveBeenCalledWith(undefined);
    });

    it('propagates the phone number if it is valid.', () => {
      component.phone = '916 221-3110 ext. 21';
      component.doChange();

      expect(changeSpy).toHaveBeenCalledWith('916 221-3110 ext. 21');
    });
  });
});
