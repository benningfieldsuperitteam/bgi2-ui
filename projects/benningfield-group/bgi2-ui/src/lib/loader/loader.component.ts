import { Component, Input, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'bgi-loader',
  templateUrl: './loader.component.html'
})
export class LoaderComponent implements OnChanges {
  @Input() when: Observable<any> | boolean;
  loading: boolean = false;

  ngOnChanges(): void {
    if (this.when instanceof Observable) {
      this.loading = true;

      this.when
        .subscribe(
          () => this.loading = false,
          () => this.loading = false
        );
    }
    else
      this.loading = !!this.when;
  }
}
