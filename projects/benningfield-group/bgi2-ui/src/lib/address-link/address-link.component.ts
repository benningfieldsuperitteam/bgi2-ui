import { Component, Input } from '@angular/core';

@Component({
  selector: 'bgi-address-link',
  templateUrl: './address-link.component.html',
})
export class AddressLinkComponent {
  @Input() address: string;
  @Input() city: string;
  @Input() state: string;
  @Input() zip: string;

  constructor() { }

}
