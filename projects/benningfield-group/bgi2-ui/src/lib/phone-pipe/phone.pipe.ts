import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'phone'})
export class PhonePipe implements PipeTransform {
  transform(value: string): string {
    if (!value)
      return value;

    const digits = value.replace(/[^\d]/g, '');

    if (digits.length < 10) {
      // 911?
      return value;
    }
    else if (digits.length === 10) {
      // Standard 10-digit (123) 456-7890.
      return digits.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1) $2-$3');
    }
    else {
      // Number with extension (123) 456-7890 ext. 22.
      return digits.replace(/^(\d{3})(\d{3})(\d{4})(\d+)$/, '($1) $2-$3 ext. $4');
    }
  }
}
