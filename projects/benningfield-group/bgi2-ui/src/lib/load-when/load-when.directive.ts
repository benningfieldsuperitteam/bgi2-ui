import { Directive, OnInit, OnChanges, Input, Renderer2, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';

@Directive({
  selector: '[bgiLoadWhen]',
})
export class LoadWhenDirective implements OnInit, OnChanges {
  @Input() bgiLoadWhen: Observable<any>;

  constructor(
    private renderer: Renderer2,
    private eleRef: ElementRef) {
  }

  ngOnInit(): void {
    // This puts an <i class="loading-spinner"></i> inside the button element.
    // It also add a "loading-button" class to the button.
    const spinner = this.renderer.createElement('i');

    this.renderer.addClass(spinner, 'loading-spinner');
    this.renderer.addClass(this.eleRef.nativeElement, 'loading-button');
    this.renderer.appendChild(this.eleRef.nativeElement, spinner);
  }

  // If the "loadWhen" observable changes, start loading.
  ngOnChanges(): void {
    if (this.bgiLoadWhen) {
      this.startLoading();

      this.bgiLoadWhen
        .subscribe(
          () => this.stopLoading(),
          () => this.stopLoading()
        );
    }
  }

  // Disable the button and set a "loading" class on it.
  private startLoading() {
    this.renderer.setAttribute(this.eleRef.nativeElement, 'disabled', 'disabled');
    this.renderer.addClass(this.eleRef.nativeElement, 'loading');
    this.renderer.addClass(this.eleRef.nativeElement, 'pointer-events-none');
  }

  // Re-enable the button and clear the "loading" class.
  private stopLoading() {
    this.renderer.removeAttribute(this.eleRef.nativeElement, 'disabled');
    this.renderer.removeClass(this.eleRef.nativeElement, 'loading');
    this.renderer.removeClass(this.eleRef.nativeElement, 'pointer-events-none');
  }
}
