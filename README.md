# @benningfield-group/bgi2-ui

UI Angular module for shared user interface functionality.

### Publishing

1. Bump the version in `package.json` and
   `projects/benningfield-group/bgi2-ui/package.json`
2. Make sure that the anything new is exported in
   `projects/benningfield-group/bgi2-ui/src/public-api.ts`
2. Lint the project: `npm run lint`
3. Test the project: `npm run test`
4. Build the project: `npm run build`
5. Publish the project: `pushd dist/benningfield-group/bgi2-ui && npm publish && popd`
